import java.util.List;
import java.util.Scanner;

public class Main {
    static final String DATABASE_URL = "jdbc:postgresql://localhost:5432/users_db";
    static final String USER = "darustur";
    static final String PASSWORD = "darustur";

    public static void main(String[] args) {
        DbManager dbManager = new DbManager(DATABASE_URL, USER, PASSWORD);
        Scanner scanner = new Scanner(System.in);

        dbManager.connect();
        dbManager.createTableIfNotExists();

        String starterMessage = "Hi, enter action number.\n1: Create new user.\n" +
                "2: View list of users.\n3: Clear Table.\n4: Exit.";
        boolean alive = true;

        while (alive) {
            System.out.println(starterMessage);
            int action = scanner.nextInt();
            String messageResponse;
            switch (action) {
                case 1:
                    User user = User.consoleInput(scanner);
                    boolean isUserAdded = dbManager.addUser(user);
                    messageResponse = isUserAdded ? "New user added successfully" : "Some error occurs";
                    break;
                case 2:
                    List<User> users = dbManager.getUsers();
                    messageResponse = User.prettyUsersInfo(users);
                    break;
                case 3:
                    dbManager.clearTable();
                    messageResponse = "Done";
                    break;
                case 4:
                    alive = false;
                    messageResponse = "Bye(";
                    break;
                default:
                    messageResponse = "Error";
                    break;
            }
            System.out.println(messageResponse);
        }
    }
}
