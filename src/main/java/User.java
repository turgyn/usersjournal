import java.util.List;
import java.util.Scanner;

public class User {
    private String user_id;
    private String firstName;
    private String lastName;
    private String birthDate;
    private String gender;

    public User() {}

    public User(String firstName, String lastName, String birthDate, String gender) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        setGender(gender);
    }

    public User(List<String> args) {
        this(args.get(1), args.get(2), args.get(3), args.get(4));
        user_id = args.get(0);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        // TODO: date validator
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        if (!gender.equals("Male") && !gender.equals("Female")) {
            gender = null;
        }
        this.gender = gender;
    }

    public static User consoleInput(Scanner scanner) {
        User user = new User();
        System.out.print("Enter First Name: ");
        user.firstName = scanner.next();
        System.out.print("Enter Last Name: ");
        user.lastName = scanner.next();
        System.out.println("Enter birth date in 'yyyy-mm-dd' format: ");
        user.birthDate = scanner.next();
        System.out.println("Choose number of your gender [1]Male/[2]Female/[3]Prefer Not to say");
        String gen_num = scanner.next();
        if (gen_num.equals("1")) {
            user.gender = "Male";
        } else if (gen_num.equals("2")) {
            user.gender = "Female";
        }
        return user;
    }

    public String toString() {
        return user_id + "\t" + firstName + "\t" + lastName + "\t" + birthDate + "\t" + gender;
    }

    public static String prettyUsersInfo(List<User> users) {
        StringBuilder res = new StringBuilder("id\tfirst_name\tlast_name\tbirth_date\tgender\n");
        for (User use: users) {
            res.append(use).append("\n");
        }
        return res.toString();
    }
}
