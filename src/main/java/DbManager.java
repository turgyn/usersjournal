import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DbManager {
    private Connection connection;
    private Statement statement;
    private String url;
    private String user;
    private String password;

    DbManager(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    public void connect() {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, user, password);
            statement = connection.createStatement();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public void createTableIfNotExists() {
        String sql = "CREATE TABLE IF NOT EXISTS users (" +
                "user_id serial PRIMARY KEY," +
                "first_name VARCHAR(50)," +
                "last_name VARCHAR(50)," +
                "birth_date DATE," +
                "gender VARCHAR(10));";
        try {
            statement.execute(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public boolean addUser(User user) {
        String sql = String.format(
                "INSERT INTO users (first_name, last_name, birth_date, gender) VALUES ('%s', '%s', '%s', '%s');",
                user.getFirstName(), user.getLastName(), user.getBirthDate(), user.getGender());
        try {
            statement.execute(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
        return true;
    }


    public List<User> getUsers() {
        String sql = "SELECT * FROM users";
        List<User> users = new ArrayList<>();

        try {
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                List<String> row = new ArrayList<>();
                for (int i = 1; i <= 5; i++) {
                    row.add(resultSet.getString(i));
                }
                users.add(new User(row));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    public void clearTable() {
        String sql = "DELETE FROM users";
        try {
            statement.execute(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
